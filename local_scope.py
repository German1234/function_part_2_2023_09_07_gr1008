from numbers import Number


def square(base: Number) -> Number:
    result = base ** 2
    print(f"The square of {base} is: {result}")


def cube(base: Number) -> Number:
    result = base ** 3
    print(f"The cube of {base} is: {result}")
